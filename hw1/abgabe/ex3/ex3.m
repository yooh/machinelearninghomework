clear all;
close all;

w =  [0.5 0.5];
std = [5 5];
mean = [5 15];
%gauss = @(i,x) 1./std(i)./sqrt(2*pi) .* exp(-1./2.*((x-mean(i))./std(i)).^2);
%g = @(i,x) log(gauss(i,x))+log(w(i));
g = @(i,x) -0.5*log(2*pi)-log(std(i))-(x-mean(i)).^2./(2.*std(i).^2)+log(w(i));


r = -10:0.1:50;

y1=pdf('Normal',r,mean(1),std(1));
y2=pdf('Normal',r,mean(2),std(2));

%find separating surface
syms x;
s = solve (g(1,x)==g(2,x),x);

%plot
% plot(r,y1);
% hold on;
% plot(r,y2);
% line([s s],[0 max(y2)]);

plot(r,g(1,r));
hold on;
plot(r,g(2,r));
vline(s);
%line([s s],[0 max(y2)]);

