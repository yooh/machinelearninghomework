%meanStd(x): Computes mean and std of a column vector
function [m, s] = meanStd(x)
xnow=x; 
%If not a column vector, use the column vector
if (size(x,1)==1)
    xnow=x';
end
m = mean(xnow) ;
s = std(xnow) ; 