%merkle@in.tum.de

close all;
clear all;

% define number of folds
numfold = 10;
% new random order or load predetermined order from file?
random = 0;

% load data
data=importdata('X.txt');

setsize = floor(size(data,1)/numfold);
leftover = mod(size(data,1),numfold);
if (random) 
    order = randperm(size(data,1));
else
    order = importdata('goodorder.txt');
end



for i = 1:numfold
   sets_v{i}=data(order((i-1)*setsize+1:i*setsize),:);
   tmp = data;
   tmp(order((i-1)*setsize+1:i*setsize),:)=[];
   sets_t{i}=tmp;
end
%in case size(data,1)/numfold has a remainder
if (leftover>0)
    sets_v{numfold} = [sets_v{numfold}; data(order(size(order,2)+1-leftover:end),:)];
    tmp = sets_t{numfold};
    tmp(size(tmp,1)+1-leftover:end,:)=[];
    sets_t{numfold}=tmp;
end

% calculate mean and std dev of each set
meanstdin=[];
meanstdout=[];
for i=1:numfold
	%training set
    [a b] = meanStd(sets_t{i});
	%validation set
	[c d] = meanStd(sets_v{i});
    meanstdin=[meanstdin;[a(1) b(1) c(1) d(1)]];
    meanstdout=[meanstdout;[a(2) b(2) c(2) d(2)]];
end

% normalization by subtracting mean and dividing by std
for i=1:numfold
	%training sets
   set = sets_t{i};
   set_in = (set(:,1)-meanstdin(i,1))./meanstdin(i,2);
   set_out = (set(:,2)-meanstdout(i,1))./meanstdout(i,2);
   sets_t_n{i} = [set_in set_out];
   %validation sets
   set = sets_v{i};
   set_in = (set(:,1)-meanstdin(i,3))./meanstdin(i,4);
   set_out = (set(:,2)-meanstdout(i,3))./meanstdout(i,4);
   sets_v_n{i} = [set_in set_out];
end

err = @(vals,w) (1/size(vals,1))*sum((vals(:,2)-polyval(w,vals(:,1))).^2);

%polynomial regression
hypo = [1 3 5 50];

%polynomial coefficients w0 w1...
m_w = zeros(max(hypo)+1,numfold,size(hypo,2));
m_w_poly = zeros(max(hypo)+1,numfold,size(hypo,2));

%error values
tr_err = zeros(numfold,size(hypo,2));
tr_err_poly = zeros(numfold,size(hypo,2));
val_err = zeros(numfold,size(hypo,2));
val_err_poly = zeros(numfold,size(hypo,2));

%choose set
for i = 1:numfold
    set = sets_t_n{i};
    %set = sets_t{i};
    
    v_y = set(:,2);
    %choose hypothesis
    for j=1:size(hypo,2)
        %build x matrix (1 x x^2 x^3 ...)
        v_x = ones(size(set,1),1);
        for k=1:hypo(j)
            v_x = [v_x set(:,1).^k];
        end
        
        v_w = pinv(v_x'*v_x)*v_x'*v_y;
        %v_w saves the values in the wrong order for using polyval(), so it needs to be reversed
        v_w = v_w(end:-1:1);
        %save calculated values 
        m_w(1:size(v_w,1),i,j) = v_w;
        
        %training error
        
        %tr_err(i,j) = (1/size(set,1))*sum((set(:,2)-polyval(v_w,set(:,1))).^2);
        tr_err(i,j) = err(set,v_w);
        %tr_err(i,j) = err(set,m_w(1:size(v_w,1),i,j));
        val_err(i,j) = err(sets_v_n{i},v_w);
        
        %compare with polyfit
%         poly_w = polyfit(set(:,1)',v_y',hypo(j));
%         
%         m_w_poly(1:size(v_w,1),i,j) = poly_w;
%         tr_err_poly(i,j) = err(set,poly_w);
%         val_err_poly(i,j) = err(sets_v_n{i},poly_w);
    end
end

% determine best hypothesis based on training error 
[min_v min_i] = min(tr_err);

best_hypos = [];
best_err = zeros(numfold,size(hypo,2));
for i=1:size(hypo,2)
    %best hypothesis
    best_hypos = [best_hypos m_w(:,min_i(i),i)];
    %calculate errors over 10-folds
    
end

%calculate errors over 10-folds
for i=1:numfold
    set = sets_v_n{i};
    for j=1:size(hypo,2)
        best_err(i,j) = err(set,best_hypos(1:hypo(j)+1,j));
    end
end

%plot mean and std/sqrt(10)
[best_err_mean best_err_std] = meanStd(best_err);
errorbar(1:4,best_err_mean, best_err_std./sqrt(10));

%plot for each class each trained hypothesis using the training values 
figure;
for i=1:size(hypo,2)
    subplot(2,2,i);
    hold on;
    for j=1:numfold
        set=sets_t_n{j};
        plot(set(:,1),polyval(m_w(1:hypo(i)+1,j,i),set(:,1)))
    end
    hold off;
    
    %plot 
    %plot(set(:,1),set(:,2));
%     hold on;
%     
%     hold off;
end

%old code
%plot();
%plot training values of best hypothesis and the values approximated by the
%hypthesis
% for i=1:size(hypo,2)
%     subplot(2,2,i);
%     set=sets_t_n{min_i(i)};
%     %plot 
%     plot(set(:,1),set(:,2));
%     hold on;
%     plot(set(:,1),polyval(m_w(1:hypo(i)+1,min_i(i),i),set(:,1)),'r')
%     hold off;
% end

% %calculate mean of training and validation error for all sets

% [tr_err_mean tr_err_std] = meanStd(tr_err);
% [val_err_mean val_err_std] = meanStd(val_err);
% 
% errorbar(tr_err_mean,tr_err_std./sqrt(10));
% errorbar(val_err_mean,val_err_std./sqrt(10));
%TODO: multiple plots at once



%set = sets_t_n{1};
%plot(set(:,1),polyval(m_w(1:6,1,3),set(:,1)),set(:,1),set(:,2));
%close all;
% for j = 1:size(hypo,2)
%     hold off;
%     hold on;
%     %plot given input/output values
%     %todo?
%     for k = 1:numfold
%         %plot values from hypothesis
%         set = sets_t_n{k};
%         plot(set(:,1),polyval(m_w(1:hypo(j)+1,k,j),set(:,1)));
%     end
% end

