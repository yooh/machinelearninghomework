clear all;
close all;

c1 = [2; 3; 4; 3; 4; 3; 3; 4; 5; 3; 2; 3; 4; 3; 2; 2; 3; 4; 5; 6];
c2 = [22; 23; 24; 23; 24; 23; 23; 24; 25; 23];

[mean_1 std_1] = meanStd(c1);
[mean_2 std_2] = meanStd(c2);
prior_1 = size(c1,1)/(size(c1,1)+size(c2,1));
prior_2 = size(c2,1)/(size(c1,1)+size(c2,1));

r=0:0.1:25;

y1=pdf('Normal',r,mean_1,std_1);
y2=pdf('Normal',r,mean_2,std_2);

plot(r,y1);
hold on;
plot(r,y2);

%calculate the intersection of the pdfs
gauss = @(i,x) 1./std(i)./sqrt(2*pi) .* exp(-1./2.*((x-mean(i))./std(i)).^2);

syms x;
x_r = solve(1/std_1/sqrt(2*pi) * exp(-1/2*((x-mean_1)/std_1)^2) == ...
      1/std_2/sqrt(2*pi) * exp(-1/2*((x-mean_2)/std_2)^2), x);