function output = polynomial(x,w)
%POLYNOMIAL takes value x and vector w and computes x*w_1+x^2*w_2+...
% 
output = zeros(size(x));
w = w(end:-1:1);
for i=1:size(w,1)
    output = output.*x+w(i);
    %output=output+x.^i*w(i+1);
    %horner schema, idiot!
end
%output = output+w(1);
