%Beginner Matlab exercises
%Prepared by Zehra Cataltepe for Pattern Recognition/Machine Learning classes
%cataltepe@itu.edu.tr

x1 = (-5:1)';  %initialize a vector. a column vector
x2 = (1:7)'+100;   %another initialization
x1t = x1' ;     %x1t is transpose of x1
%Commands to get the size of a vector or a matrix
size(x1)   %no of columns    
size(x1t)  
size(x1,1) %no of rows
size(x1t,1)

%Compute mean and std of each vector
[m1, s1] = meanStd(x1) ; 
m1
s1
nx1 = (x1-m1)/s1;

[m2, s2] = meanStd(x2) ; 
m2
s2
nx2 = (x2-m2)/s2;


plot(x1)
figure
subplot(2,1,1) %to plot to plots in one figure, 2 rows and 1 column
plot(x1,x2,'ro')
xlabel('x1') ; 
ylabel('x2') ; 
title('Unnormalized x1 and x2, x1 has much larger values than x2')

subplot(2,1,2)
plot(nx1,nx2,'bs')
xlabel('normalized x1') ; 
ylabel('normalized x2') ;
title('Normalized x1 and x2, x1 and x2 in the same range')

axis([-2 2 -2 2]) % to adjust min and max values of both x and y axis
%hold on % to be able to plot something else on the same plot. Use hold off to go back to normal

%Useful commands
%
%help plot 
%lookfor rand
%to get help on any command use help
%to look for a keyword in commands, use  lookfor _name_

%use these commands to clean up
%close all  %close all windows
%clear all  %clear all variables


