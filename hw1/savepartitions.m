close all;
clear all;

% define number of folds
numfold = 10;
% new random order or load predetermined order from file?
random = 0;

% load data
data=importdata('X.txt');

setsize = floor(size(data,1)/numfold);
leftover = mod(size(data,1),numfold);
if (random) 
    order = randperm(size(data,1));
else
    order = importdata('goodorder.txt');
end



for i = 1:numfold
   sets_v{i}=data(order((i-1)*setsize+1:i*setsize),:);
   tmp = data;
   tmp(order((i-1)*setsize+1:i*setsize),:)=[];
   sets_t{i}=tmp;
end
%in case size(data,1)/numfold has a remainder
if (leftover>0)
    sets_v{numfold} = [sets_v{numfold}; data(order(size(order,2)+1-leftover:end),:)];
    tmp = sets_t{numfold};
    tmp(size(tmp,1)+1-leftover:end,:)=[];
    sets_t{numfold}=tmp;
end

%write sets to file
for i = 1:numfold
    fname = strcat('set_t_',int2str(i),'.txt');
    %varname = strcat('sets_t{',int2str(i),'}');
    set = sets_t{i};
    save(fname,'set','-ascii');
    fname = strcat('set_v_',int2str(i),'.txt');
    set = sets_v{i};
    %varname = strcat('sets_v{',int2str(i),'}');
    save(fname,'set','-ascii');
end