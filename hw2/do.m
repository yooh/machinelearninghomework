clear all;
close all;

data = csvread('optdigits.tra');
tr_size = round(size(data,1)*0.9);
tr_data = data(1:tr_size,:);
val_data = data(tr_size+1:end,:);
% F = data(:,1:64);
% L = data(:,65:65);

K = 10;% number of classes
r = 1:10;
d = 64+1;% number of features+1 for generality
N = tr_size;%size of training set
eta = 1;%some factor

tr_data_pad = [ones(tr_size,1) tr_data];

%"pre"-loops
for i = 1:K
    for j = 1:d
        w(i,j) = (-1 + (rand(1) * 2))*0.01;
    end
end

%repeat/convergence loop
re = 1;
while(re<2)
    d_w = zeros(K,d);
    
    for t = 1:N
        %green loop
        for i = 1:K
            o(i)=0;
            for j = 1:d
                %update o
                o(i)=o(i)+w(i,j)*tr_data_pad(t,j);
%                 if isnan(w(i,j))
%                     w(i,j)
%                     tr_data(t,j)
%                 end
            end
        end
        for i = 1:K
            %y_i = exp...
            y(i) = exp(o(i))/(sum(exp(o(1:K))));
        end
        %red loop
        for i = 1:K
            for j=1:d
                %update d_w
                d_w(i,j) = d_w(i,j)+(r(1,i)-y(i))*tr_data_pad(t,j);
                
            end
        end
    end
    for i = 1:K
        for j=1:d
            w(i,j)=w(i,j)+eta*d_w(i,j);
        end
    end
    re = re+1;
end
%w
%training error



%validation error