close all;
clear all;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Compute the discriminant functions for both classes%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
c1=[1 1;2 1];
c2=[4 3;5 4];

m_1=[1.5;1];
m_2=[4.5;3.5];
syms x_1 x_2;
x_vec=[x_1;x_2];
g_1=@(x) (x-m_1)'*(x-m_1);
g_2=@(x) (x-m_2)'*(x-m_2);

eq =@(x) g_1(x)-g_2(x);
%sol = solve(eq,x_vec);

eq([0 29.25/5]')
eq([29.25/6 0]')
% x=0:6;
% for i=0:0.1:6
%     g_1(i)-g_2(i)
% end

% 
% 
% S_11=(1/(2.25*6.25))*[6.25 0;0 2.25];
% S_22=(1/(12.25*20.25))*[20.25 0;0 12.25];
% syms x_1 x_2;
% x_vec=[x_1;x_2];
% m_vec_1=[1.5;2.5];
% m_vec_2=[4.5;3.5];
% w_1=0.5;
% w_2=0.5;
% p_x_1=1/((2*pi)*sqrt(2.25*1))*exp(-0.5*(x_vec-m_vec_1)'*(S_11)*(x_vec-m_vec_1));
% p_x_2=1/((2*pi)*sqrt(12.25*20.25))*exp(-0.5*(x_vec-m_vec_2)'*(S_22)*(x_vec-m_vec_2));
% g_x_1=log(p_x_1)+log(w_1);
% g_x_2=log(p_x_2)+log(w_2);
% 
% x=[0:0.1:1;0:0.1:1];
% plot(x,g_x_1);
% hold on;
% plot(x,g_x_2);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Compute and draw the discriminant function that separates the two classes%%%%%%%%%%%%%%%%%%%%%%%%%%%%




% eq=g_x_1-g_x_2;
% solution=solve(eq,x_vec);
% a=solution;